import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 23.04.18
 * Time: 15:24
 * To change this template use File | Settings | File Templates.
 */
public class BaseTestsPratik7 {
    WebDriver driver = initChromeDriver();
    @Test
    public void TestPratik1(){
        driver.get("https://www.pratik.com.ua/");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement mens = driver.findElement(By.xpath("//*[@id=\"megamenu-menu\"]/div/div[3]/ul/li[2]/a"));
        mens.click();
        WebElement tufli = driver.findElement(By.xpath("//*[@id=\"megamenu-menu\"]/div/div[3]/ul/li[2]/div/div/ul/li[1]/a"));
        tufli.click();
        WebElement tuflinubuk = driver.findElement(By.xpath("//*[@id=\"res-products\"]/div[1]/div[1]/div/div[1]/a[1]/img"));
        tuflinubuk.click();
        WebElement price = driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[1]/div[2]/div[2]/span[2]"));
        String priceText = price.getText();
        assertEquals("1457 грн.", priceText);
        System.out.println("1457 uah");
        WebElement vkorzinu = driver.findElement(By.xpath("//*[@id=\"button-cart\"]"));
        vkorzinu.click();
        driver.quit();

    }

    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver() ; //To change body of created methods use File | Settings | File Templates.
    }

}
